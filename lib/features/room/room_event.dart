part of 'room_bloc.dart';

abstract class RoomEvent extends Equatable {
  const RoomEvent();

  @override
  List<Object?> get props => [];
}

// Event fired when the state of the Room is updated: from lobby to
// initialization, from playing to buffering...
class RoomStatusUpdated extends RoomEvent {
  final String status;

  const RoomStatusUpdated(this.status);

  @override
  List<Object?> get props => [status];
}

// Event fired when the current track is updated
class RoomCurrentTrackUpdated extends RoomEvent {
  final Track currentTrack;

  const RoomCurrentTrackUpdated(this.currentTrack);

  @override
  List<Object?> get props => [currentTrack];
}

// Event fired when the player is guessing a song
class RoomGuessEvent extends RoomEvent {
  final String guess;

  const RoomGuessEvent(this.guess);

  @override
  List<Object?> get props => [guess];
}

// Event fired when the result of a guess is updated
// TODO: Listen for rounds in the database
class RoomGuessResultEvent extends RoomEvent {
  final FormzStatus guessStatus;
  final PlayerResult? result;

  const RoomGuessResultEvent({required this.guessStatus, this.result});

  @override
  List<Object?> get props => [guessStatus, result];
}

// Event fired when the player want to start the Room
class RoomStartEvent extends RoomEvent {}
