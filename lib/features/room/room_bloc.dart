import 'dart:async';
import 'dart:math';

import 'package:just_audio/just_audio.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:formz/formz.dart';
import 'package:room_music_quiz/models/player_result.dart';
import 'package:room_music_quiz/models/room_status.dart';
import 'package:room_music_quiz/models/track.dart';
import 'package:room_music_quiz/repositories/auth_repository.dart';
import 'package:room_music_quiz/repositories/room_repository.dart';
import 'package:room_music_quiz/repositories/track_repository.dart';
import 'package:room_music_quiz/utils/logger.dart';

part 'room_event.dart';

part 'room_state.dart';

class RoomBloc extends Bloc<RoomEvent, RoomState> {
  RoomBloc(String roomId, RoomRepository roomRepository,
      AuthRepository authRepository, TrackRepository trackRepository)
      : _roomId = roomId,
        _roomRepository = roomRepository,
        _trackRepository = trackRepository,
        _authRepository = authRepository,
        super(RoomState()) {
    on<RoomGuessEvent>(_onSongGuessed);
    on<RoomStatusUpdated>(_onStatusUpdated);
    on<RoomCurrentTrackUpdated>(_onCurrentTrackUpdated);
    on<RoomStartEvent>((event, emit) => _roomRepository.startRoom(_roomId));

    _init();
  }

  final RoomRepository _roomRepository;
  final AuthRepository _authRepository;
  final TrackRepository _trackRepository;
  final AudioPlayer _audioPlayer = AudioPlayer();
  final String _roomId;

  late final String _user;
  late final TextEditingController _controller;
  late final StreamSubscription _roomStream;

  void _init() {
    // Add the player to the Room
    _user = _authRepository.user!;
    _roomRepository.addPlayerToRoom(_roomId, _user);

    // Listen to the database
    // TODO: Remove hard coding of Firebase
    _roomStream = FirebaseDatabase.instanceFor(
      app: FirebaseDatabase.instance.app,
      databaseURL:
          "https://rooms-music-quiz-d2dad.europe-west1.firebasedatabase.app/",
    ).ref(_roomId).onValue.listen(_onDatabaseEvent, onError: _onDatabaseError);

    // Setup the text controller
    _controller = TextEditingController();
  }

  void _onDatabaseEvent(DatabaseEvent event) async {
    // Check and update the current playing track
    if (event.snapshot.hasChild("currentlyPlaying")) {
      final newTrack = await _trackRepository.getTrackById(
          event.snapshot.child("currentlyPlaying").value as String);
      if (state.currentlyPlaying != newTrack) {
        add(RoomCurrentTrackUpdated(newTrack));
      }
    }

    // Check and update the current state of the Room
    if (event.snapshot.hasChild("state")) {
      final newStatus = event.snapshot.child("state").value as String;
      if (state.status != newStatus) {
        add(RoomStatusUpdated(newStatus));
      }
    }
  }

  Future<void> _onDatabaseError(Object e, StackTrace s) async {
    Log.e(e);
    await FirebaseCrashlytics.instance.recordError(e, s);
  }

  void _onStatusUpdated(
      RoomStatusUpdated event, Emitter<RoomState> emit) async {
    emit(
      state.copyWith(
        status: event.status,
        roundResult: PlayerResult.empty,
        guessStatus: FormzStatus.pure,
      ),
    );
    switch (event.status) {
      case RoomStatus.PLAYING:
        _audioPlayer.play();
        break;
      case RoomStatus.BUFFERING:
      case RoomStatus.INITIALIZATION:
        _controller.clear();
        await _audioPlayer.stop();
        _audioPlayer.setUrl(state.currentlyPlaying!.previewUrl!);
        break;
    }
  }

  void _onCurrentTrackUpdated(
      RoomCurrentTrackUpdated event, Emitter<RoomState> emit) {
    final previousTrack = state.currentlyPlaying;
    final previousSongs = [...state.previousSongs];
    if (previousTrack != null) previousSongs.add(previousTrack);
    emit(
      state.copyWith(
        previousSongs: previousSongs,
        currentlyPlaying: event.currentTrack,
      ),
    );
  }

  void _onSongGuessed(RoomGuessEvent event, Emitter<RoomState> emit) async {
    try {
      emit(
        state.copyWith(guessStatus: FormzStatus.submissionInProgress),
      );
      final oldResult = state.roundResult;
      final currentResult = await _roomRepository.guessTrack(
        _roomId,
        _user,
        state.currentlyPlaying!.name,
        state.currentlyPlaying!.artists.map((e) => e.name).toList(),
        event.guess,
      );

      // Keep the best results of the round
      late final PlayerResult bestResult;
      if (oldResult != PlayerResult.empty) {
        List<double> _artists = List.empty(growable: true);
        for (var i = 0; i < oldResult.artists.length; i++) {
          _artists.add(max(oldResult.artists[i], currentResult.artists[i]));
        }
        bestResult = PlayerResult(
          title: max(oldResult.title, currentResult.title),
          artists: _artists,
        );
      } else {
        bestResult = currentResult;
      }
      emit(
        state.copyWith(
          guessStatus: FormzStatus.submissionSuccess,
          roundResult: bestResult,
        ),
      );
    } catch (_) {
      emit(state.copyWith(guessStatus: FormzStatus.submissionFailure));
    }
  }

  @override
  Future<void> close() async {
    // Dispose of resources
    _audioPlayer.dispose();
    _roomStream.cancel();
    _controller.dispose();

    // Remove the player from the Room
    _roomRepository.removePlayerFromRoom(_roomId, _user);
    super.close();
  }
}
