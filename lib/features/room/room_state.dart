part of 'room_bloc.dart';

class RoomState extends Equatable {
  final String status;
  final Track? currentlyPlaying;
  final List<Track> previousSongs;
  final FormzStatus guessStatus;
  final PlayerResult roundResult;

  RoomState({
    this.currentlyPlaying,
    List<Track>? previousSongs,
    this.status = RoomStatus.LOBBY,
    this.guessStatus = FormzStatus.pure,
    this.roundResult = PlayerResult.empty,
  }) : previousSongs = previousSongs ?? List.empty();

  RoomState copyWith({
    String? status,
    Track? currentlyPlaying,
    List<Track>? previousSongs,
    FormzStatus? guessStatus,
    PlayerResult? roundResult,
  }) =>
      RoomState(
        roundResult: roundResult ?? this.roundResult,
        guessStatus: guessStatus ?? this.guessStatus,
        status: status ?? this.status,
        previousSongs: previousSongs ?? this.previousSongs,
        currentlyPlaying: currentlyPlaying ?? this.currentlyPlaying,
      );

  @override
  List<Object?> get props => [
        status,
        currentlyPlaying,
        previousSongs,
        guessStatus,
        roundResult,
      ];
}
