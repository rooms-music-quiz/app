import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:room_music_quiz/models/room.dart';
import 'package:room_music_quiz/repositories/room_repository.dart';
import 'package:room_music_quiz/utils/logger.dart';

part 'room_list_event.dart';

part 'room_list_state.dart';

class RoomListBloc extends Bloc<RoomListEvent, RoomListState> {
  RoomListBloc({
    required RoomRepository roomRepository,
  })  : _roomRepository = roomRepository,
        super(const RoomListState()) {
    on<RoomListFetched>(_onRoomListFetched);
  }

  Future<void> _onRoomListFetched(
    RoomListFetched event,
    Emitter<RoomListState> emit,
  ) async {
    try {
      final rooms = await _roomRepository.getRooms();
      emit(state.copyWith(status: RoomListStatus.success, rooms: rooms));
    } catch (e) {
      emit(state.copyWith(status: RoomListStatus.failure));
    }
  }

  final RoomRepository _roomRepository;
}
