part of 'room_list_bloc.dart';

enum RoomListStatus { initial, success, failure }

class RoomListState extends Equatable {
  const RoomListState({
    this.status = RoomListStatus.initial,
    this.rooms = const {},
  });

  final RoomListStatus status;
  final Set<Room> rooms;

  RoomListState copyWith({
    RoomListStatus? status,
    Set<Room>? rooms,
  }) =>
      RoomListState(
        status: status ?? this.status,
        rooms: rooms ?? this.rooms,
      );

  @override
  List<Object> get props => [status, rooms];
}
