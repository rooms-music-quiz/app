part of 'room_creation_bloc.dart';

abstract class RoomCreationEvent extends Equatable {
  const RoomCreationEvent();

  @override
  List<Object?> get props => [];
}

class RoomCreationLimitUpdated extends RoomCreationEvent {
  final int limit;

  const RoomCreationLimitUpdated(this.limit);

  @override
  List<Object?> get props => [limit];
}

class RoomCreationDifficultyUpdated extends RoomCreationEvent {
  final int difficulty;

  const RoomCreationDifficultyUpdated(this.difficulty);

  @override
  List<Object?> get props => [difficulty];
}

class RoomCreationGenresUpdated extends RoomCreationEvent {
  final List<String> genres;

  const RoomCreationGenresUpdated(this.genres);

  @override
  List<Object?> get props => [genres];
}

class RoomCreationSubmitted extends RoomCreationEvent {
  const RoomCreationSubmitted();
}
