import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:room_music_quiz/models/room_creation/difficulty.dart';
import 'package:room_music_quiz/models/room_creation/genres.dart';
import 'package:room_music_quiz/models/room_creation/limit.dart';
import 'package:room_music_quiz/repositories/room_repository.dart';
import 'package:room_music_quiz/utils/logger.dart';

part 'room_creation_event.dart';

part 'room_creation_state.dart';

class RoomCreationBloc extends Bloc<RoomCreationEvent, RoomCreationState> {
  RoomCreationBloc(RoomRepository roomRepository)
      : _roomRepository = roomRepository,
        super(const RoomCreationState()) {
    on<RoomCreationLimitUpdated>(_onRoomCreationLimitUpdated);
    on<RoomCreationDifficultyUpdated>(_onRoomCreationDifficultyUpdated);
    on<RoomCreationGenresUpdated>(_onRoomCreationGenresUpdated);
    on<RoomCreationSubmitted>(_onRoomCreationSubmitted);
  }

  final RoomRepository _roomRepository;

  void _onRoomCreationLimitUpdated(
    RoomCreationLimitUpdated event,
    Emitter<RoomCreationState> emit,
  ) {
    final limit = Limit.dirty(event.limit);
    emit(
      state.copyWith(
        limit: limit,
        status: Formz.validate([limit, state.difficulty, state.genres]),
      ),
    );
  }

  void _onRoomCreationDifficultyUpdated(
    RoomCreationDifficultyUpdated event,
    Emitter<RoomCreationState> emit,
  ) {
    final difficulty = Difficulty.dirty(event.difficulty);
    emit(
      state.copyWith(
        difficulty: difficulty,
        status: Formz.validate([state.limit, difficulty, state.genres]),
      ),
    );
  }

  void _onRoomCreationGenresUpdated(
    RoomCreationGenresUpdated event,
    Emitter<RoomCreationState> emit,
  ) {
    final genres = Genres.dirty(event.genres);
    emit(
      state.copyWith(
        genres: genres,
        status: Formz.validate([state.limit, state.difficulty, genres]),
      ),
    );
  }

  void _onRoomCreationSubmitted(
    RoomCreationSubmitted event,
    Emitter<RoomCreationState> emit,
  ) async {
    try {
      emit(state.copyWith(status: FormzStatus.submissionInProgress));
      final roomId = await _roomRepository.createRoom(state.toMap());
      emit(
        state.copyWith(
          status: FormzStatus.submissionSuccess,
          roomId: roomId,
        ),
      );
    } catch (_) {
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    }
  }
}
