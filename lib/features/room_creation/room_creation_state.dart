part of 'room_creation_bloc.dart';

class RoomCreationState extends Equatable {
  final FormzStatus status;
  final Limit limit;
  final Difficulty difficulty;
  final Genres genres;
  final String? roomId;

  const RoomCreationState({
    this.status = FormzStatus.pure,
    this.limit = const Limit.pure(),
    this.difficulty = const Difficulty.pure(),
    this.genres = const Genres.pure(),
    this.roomId,
  });

  RoomCreationState copyWith({
    FormzStatus? status,
    Limit? limit,
    Difficulty? difficulty,
    Genres? genres,
    String? roomId,
  }) {
    return RoomCreationState(
      status: status ?? this.status,
      limit: limit ?? this.limit,
      difficulty: difficulty ?? this.difficulty,
      genres: genres ?? this.genres,
      roomId: roomId ?? this.roomId,
    );
  }

  Map<String, String> toMap() => {
        "difficulty": difficulty.value.toString(),
        "genres": genres.value.join(","),
        "numbers": limit.value.toString(),
        "isPublic": "true",
      };

  @override
  List<Object> get props => [status, limit, difficulty, genres];
}
