import 'dart:convert';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:graphql/client.dart';
import 'package:http/http.dart';
import 'package:room_music_quiz/graphql/client_factory.dart';
import 'package:room_music_quiz/graphql/track_queries.dart';
import 'package:room_music_quiz/models/track.dart';
import 'package:room_music_quiz/utils/logger.dart';

import '../models/playlist.dart';

class TrackRepository {
  final GraphQLClient _graphClient;

  TrackRepository({GraphQLClient? graphClient})
      : _graphClient = graphClient ?? ClientFactory();

  Future<Track> getTrackById(String id) async {
    try {
      final QueryOptions options = QueryOptions(
        document: gql(TrackQueries.getTrack),
        variables: <String, dynamic>{
          'trackId': id,
        },
      );
      final QueryResult result = await _graphClient.query(options);
      if (result.hasException) throw result.exception!;
      return Track.fromJson(result.data!["track"]);
    } catch (e, s) {
      Log.e("Error while getting track $id", e, s);
      rethrow;
    }
  }
}
