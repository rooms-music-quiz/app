import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:room_music_quiz/utils/logger.dart';

enum AuthenticationStatus { unknown, authenticated, unauthenticated }
enum AuthenticationMethods { anonymous, google, facebook, apple }

class AuthRepository {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late final StreamController<AuthenticationStatus> _statusController;

  String? user;

  AuthRepository._() {
    _statusController = StreamController<AuthenticationStatus>(
      onListen: () {
        _auth.userChanges().listen((User? user) async {
          if (user == null) {
            _statusController.add(AuthenticationStatus.unauthenticated);
            this.user = null;
          } else {
            await FirebaseCrashlytics.instance.setUserIdentifier(user.uid);
            await FirebaseAnalytics.instance.setUserId(id: user.uid);
            _statusController.add(AuthenticationStatus.authenticated);
            this.user = user.uid;
          }
        });
      },
    );
  }

  static final AuthRepository _instance = AuthRepository._();

  factory AuthRepository() => _instance;

  Stream<AuthenticationStatus> getStatus() => _statusController.stream;

  Future<void> login(AuthenticationMethods signInMethod) async {
    UserCredential? credential;
    switch (signInMethod) {
      case AuthenticationMethods.anonymous:
        //credential = await _googleGameSignIn();
        //if (credential == null) await _auth.signInAnonymously();
        await _auth.signInAnonymously();
        break;
      case AuthenticationMethods.google:
        credential = await _googleSignIn();
        break;
      case AuthenticationMethods.facebook:
        credential = await _facebookSignIn();
        break;
      case AuthenticationMethods.apple:
        // TODO: Handle this case.
        break;
    }
  }

  Future<String?> getJWT() async => await _auth.currentUser?.getIdToken();

  Future<void> logout() async => await _auth.signOut();

  Future<UserCredential?> _googleGameSignIn() async {
    final GoogleSignIn googleSignIn = GoogleSignIn(
      signInOption: SignInOption.games,
      scopes: ['email'],
    );

    final GoogleSignInAccount? googleUser = await googleSignIn.signIn();
    if (googleUser == null) return null;

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    return await _auth.signInWithCredential(credential);
  }

  Future<UserCredential> _googleSignIn() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth =
        await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    return await _auth.signInWithCredential(credential);
  }

  Future<UserCredential> _facebookSignIn() async {
    // Trigger the sign-in flow
    final LoginResult loginResult = await FacebookAuth.instance.login();

    final token = loginResult.accessToken?.token;

    if (loginResult.status == LoginStatus.success) {
      // you are logged
      final AccessToken accessToken = loginResult.accessToken!;

      // Create a credential from the access token
      final OAuthCredential facebookAuthCredential =
          FacebookAuthProvider.credential(accessToken.token);

      // Once signed in, return the UserCredential
      return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
    } else {
      Log.e("${loginResult.status} : ${loginResult.message}");
      throw Exception(loginResult.message);
    }
  }

  void dispose() => _statusController.close();
}
