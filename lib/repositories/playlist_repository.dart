import 'dart:convert';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:room_music_quiz/utils/logger.dart';

import '../models/playlist.dart';

class PlaylistRepository {
  static const String HOST = "192.168.1.141:8080";

  final Client _httpClient;
  final String _language;
  final String _country;

  PlaylistRepository({Client? client, Locale? locale})
      : _httpClient = client ?? Client(),
        _language = locale?.languageCode ?? 'en',
        _country = locale?.countryCode ?? "US";

  Future<Set<Playlist>> getFeaturedPlaylists() async {
    final url = Uri.http(HOST, "playlists/featured", {
      "country": _country,
      "locale": "${_language}_$_country",
    });
    try {
      final response = await _httpClient.get(url);
      return compute(_parsePlaylists, response.body);
    } catch (e, s) {
      Log.e("Error while getting featured playlist", e, s);
      rethrow;
    }
  }

  Future<Playlist> createPlaylist(
      Map<String, String> playlistParameters) async {
    final url = Uri.http(HOST, "playlists");
    try {
      final response = await _httpClient.post(url, body: playlistParameters);
      return Playlist.fromJson(jsonDecode(response.body));
    } catch (e, s) {
      Log.e("Error while creating playlist", e, s);
      rethrow;
    }
  }

  static Set<Playlist> _parsePlaylists(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Playlist>((json) => Playlist.fromJson(json)).toSet();
  }
}
