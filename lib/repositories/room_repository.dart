import 'package:flutter/foundation.dart';
import 'package:graphql/client.dart';
import 'package:http/http.dart';
import 'package:room_music_quiz/graphql/client_factory.dart';
import 'package:room_music_quiz/graphql/room_mutations.dart';
import 'package:room_music_quiz/graphql/room_queries.dart';
import 'package:room_music_quiz/models/player_result.dart';
import 'package:room_music_quiz/utils/logger.dart';

import '../models/room.dart';

class RoomRepository {
  final GraphQLClient _graphClient;

  RoomRepository({GraphQLClient? graphClient})
      : _graphClient = graphClient ?? ClientFactory();

  Future<Set<Room>> getRooms() async {
    try {
      final QueryOptions options = QueryOptions(
        document: gql(RoomQueries.getRooms),
      );
      final QueryResult result = await _graphClient.query(options);
      if (result.hasException) throw result.exception!;
      return _parseRoom(result.data!);
      // return compute(_parseRoom, result.data!);
    } catch (e, s) {
      Log.e("Error while getting rooms", e, s);
      rethrow;
    }
  }

  Future<Room> getRoom(String id) async {
    try {
      final QueryOptions options = QueryOptions(
        document: gql(RoomQueries.getRoom),
        variables: <String, dynamic>{
          'roomId': id,
        },
      );
      final QueryResult result = await _graphClient.query(options);
      if (result.hasException) throw result.exception!;
      return Room.fromJson(result.data!["room"]);
    } catch (e, s) {
      Log.e("Error while getting rooms", e, s);
      rethrow;
    }
  }

  Future<String> createRoom(Map<String, String> roomParameters) async {
    try {
      final MutationOptions options = MutationOptions(
        document: gql(RoomMutations.createRoom),
        variables: <String, dynamic>{
          'params': roomParameters,
        },
      );
      final QueryResult result = await _graphClient.mutate(options);
      if (result.hasException) throw result.exception!;
      return result.data!['createRoom']['id'];
    } catch (e, s) {
      Log.e("Error while creating room", e, s);
      rethrow;
    }
  }

  Future<void> addPlayerToRoom(String roomId, String playerId) async {
    try {
      final MutationOptions options = MutationOptions(
        document: gql(RoomMutations.addPlayerToRoom),
        variables: <String, dynamic>{
          'roomId': roomId,
          'playerId': playerId,
        },
      );
      final QueryResult result = await _graphClient.mutate(options);
      if (result.hasException) throw result.exception!;
    } catch (e, s) {
      Log.e("Error while creating room", e, s);
      rethrow;
    }
  }

  Future<void> removePlayerFromRoom(String roomId, String playerId) async {
    try {
      final MutationOptions options = MutationOptions(
        document: gql(RoomMutations.removePlayerFromRoom),
        variables: <String, dynamic>{
          'roomId': roomId,
          'playerId': playerId,
        },
      );
      final QueryResult result = await _graphClient.mutate(options);
      if (result.hasException) throw result.exception!;
    } catch (e, s) {
      Log.e("Error while creating room", e, s);
      rethrow;
    }
  }

  Future<void> startRoom(String roomId) async {
    try {
      final MutationOptions options = MutationOptions(
        document: gql(RoomMutations.startRoom),
        variables: <String, dynamic>{
          'roomId': roomId,
        },
      );
      final QueryResult result = await _graphClient.mutate(options);
      if (result.hasException) throw result.exception!;
    } catch (e, s) {
      Log.e("Error while creating room", e, s);
      rethrow;
    }
  }

  Future<PlayerResult> guessTrack(String roomId, String playerId, String title,
      List<String> artists, String input) async {
    try {
      final MutationOptions options = MutationOptions(
          document: gql(RoomMutations.guessTrack),
          variables: <String, dynamic>{
            "roomId": roomId,
            "playerId": playerId,
            "title": title,
            "artists": artists,
            "input": input
          });
      final QueryResult result = await _graphClient.mutate(options);
      if (result.hasException) throw result.exception!;
      return PlayerResult.fromJson(result.data!["guessTrack"]);
    } catch (e, s) {
      Log.e("Error while creating room", e, s);
      rethrow;
    }
  }

  static Set<Room> _parseRoom(Map<String, dynamic> responseBody) {
    return (responseBody['rooms'] as List<dynamic>)
        .map<Room>((json) => Room.fromJson(json))
        .toSet();
  }
}
