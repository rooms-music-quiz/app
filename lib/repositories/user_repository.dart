import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:room_music_quiz/models/user.dart';

class UserRepository {
  UserRepository._();

  static final UserRepository _instance = UserRepository._();

  factory UserRepository() => _instance;

  User? user;

  Future<User?> getUser() async {
    final firebaseUser = auth.FirebaseAuth.instance.currentUser;
    return _fromFirebaseUser(firebaseUser);
  }

  User? _fromFirebaseUser(auth.User? firebaseUser) {
    if (firebaseUser == null) {
      return null;
    } else {
      return User(
        id: firebaseUser.uid,
        email: firebaseUser.email,
        emailVerified: firebaseUser.emailVerified,
        name: firebaseUser.displayName,
        previewUrl: firebaseUser.photoURL,
        phoneNumber: firebaseUser.phoneNumber,
      );
    }
  }
}
