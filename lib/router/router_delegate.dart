import 'package:flutter/material.dart';
import 'package:room_music_quiz/pages/featured_playlist/featured_playlist_view.dart';
import 'package:room_music_quiz/pages/login/login_view.dart';
import 'package:room_music_quiz/pages/room/room.dart';
import 'package:room_music_quiz/pages/room_creation/room_creation.dart';
import 'package:room_music_quiz/pages/room_list/room_list.dart';
import 'package:room_music_quiz/pages/splashscreen/splashscreen.dart';

class MyRouterDelegate extends RouterDelegate<List<RouteSettings>>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<List<RouteSettings>> {
  MyRouterDelegate._();

  static final MyRouterDelegate _instance = MyRouterDelegate._();

  factory MyRouterDelegate() => _instance;

  final List<Page<dynamic>> _pages = [];

  @override
  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  Future<void> setNewRoutePath(List<RouteSettings> configuration) async {}

  @override
  Future<bool> popRoute() {
    if (_pages.length > 1) {
      _pages.removeLast();
      notifyListeners();
      return Future.value(true);
    }
    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: List.of(_pages),
      onPopPage: _onPopPage,
    );
  }

  bool _onPopPage(Route route, dynamic result) {
    if (!route.didPop(result)) return false;
    popRoute();
    return true;
  }

  MaterialPage _createPage(RouteSettings routeSettings) {
    Widget child;

    if (routeSettings.name == null) {
      throw (Exception("Path ${routeSettings.name} not found"));
    }

    final uri = Uri.parse(routeSettings.name!);
    String root;

    if (uri.pathSegments.isNotEmpty) {
      root = uri.pathSegments.first;
    } else {
      root = '';
    }

    switch (root) {
      case '':
        child = const SplashScreen();
        break;
      case 'login':
        child = const LoginView();
        break;
      case 'room':
        final id = uri.pathSegments[1];
        child = Room(id: id);
        break;
      case 'rooms':
        if (uri.pathSegments.length == 1) {
          child = const RoomList();
        } else {
          child = const RoomCreation();
        }
        break;
      case 'playlists/featured':
        child = const FeaturedPlaylist();
        break;
      default:
        throw (Exception("Path ${routeSettings.name} not found"));
    }

    return MaterialPage(
      child: child,
      key: ValueKey(routeSettings.name!),
      name: routeSettings.name,
      arguments: routeSettings.arguments,
    );
  }

  void pushPage({required String name, dynamic arguments}) {
    _pages.add(_createPage(RouteSettings(name: name, arguments: arguments)));
    notifyListeners();
  }

  void replace({required String name, dynamic arguments}) {
    _pages.removeLast();
    pushPage(name: name, arguments: arguments);
  }

  void replaceAll({required String name, dynamic arguments}) {
    _pages.clear();
    pushPage(name: name, arguments: arguments);
  }
}
