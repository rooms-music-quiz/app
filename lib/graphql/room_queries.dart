class RoomQueries {
  static const getRoom = r'''
  query getRoom($roomId: String!) {
    room(id: $roomId) {
      id
      state
      currentlyPlaying {
        id
        name
        previewUrl
        images {
          url
          height
          width
        }
        artists {
          id
          name
        }
      }
      playlist {
        id
        name
        description
        images {
          url
          height
          width
        }
        tracks {
          id
          name
          previewUrl
          images {
            url
            height
            width
          }
          artists {
            id
            name
          }
        }
      }
    }
  }
  ''';

  static const getRooms = r'''
{
  rooms {
    id
    state
    currentlyPlaying {
      id
      name
      previewUrl
      images {
        url
        height
        width
      }
      artists {
        id
        name
      }
    }
    playlist {
      id
      name
      description
      images {
        url
        height
        width
      }
      tracks {
        id
        name
        previewUrl
        images {
          url
          height
          width
        }
        artists {
          id
          name
        }
      }
    }
  }
}
  ''';
}
