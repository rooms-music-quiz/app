import 'package:graphql/client.dart';
import 'package:room_music_quiz/repositories/auth_repository.dart';
import 'package:room_music_quiz/utils/logger.dart';

class ClientFactory extends GraphQLClient {
  static const String _HOST = "http://192.168.1.142:8000/graphql";
   // static const String _HOST = "https://music-quiz-d2dad.ew.r.appspot.com/graphql";

  ClientFactory._()
      : super(
          cache: GraphQLCache(),
          link: AuthLink(getToken: () async {
            final jwt = await AuthRepository().getJWT();
            return 'Bearer $jwt';
          }).concat(HttpLink(_HOST)),
        );

  static final ClientFactory _instance = ClientFactory._();

  factory ClientFactory() => _instance;
}
