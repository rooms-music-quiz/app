class RoomMutations {
  static const createRoom = r'''
  mutation createRoom($params: RoomParametersInput!) {
    createRoom(params: $params) {
      id
    }
  }
  ''';

  static const addPlayerToRoom = r'''
  mutation addPlayerToRoom($roomId: String!, $playerId: String!) {
    addPlayerToRoom(roomId: $roomId, playerId: $playerId)
  }
  ''';

  static const guessTrack = r'''
  mutation guessTrack($roomId: String! $playerId: String!, $title: String!, $artists: [String!]!, $input: String!) {
    guessTrack(roomId: $roomId, playerId: $playerId, title: $title, artists: $artists, input: $input) {
      track,
      artists,
    }
  }
  ''';

  static const removePlayerFromRoom = r'''
  mutation removePlayerFromRoom($roomId: String!, $playerId: String!) {
    removePlayerFromRoom(roomId: $roomId, playerId: $playerId)
  }
  ''';

  static const startRoom = r'''
  mutation startRoom($roomId: String!) {
    startRoom(roomId: $roomId)
  }
  ''';
}
