class TrackQueries {
  static const getTrack = r'''
  query getTrack($trackId: String!) {
    track(id: $trackId) {
      id
      name
      previewUrl
      images {
        url
        height
        width
      }
      artists {
        id
        name
      }
    }
  }
  ''';
}
