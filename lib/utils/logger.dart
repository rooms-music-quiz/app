import 'package:logger/logger.dart';

class Log {
  static final _logger = Logger();

  static v(dynamic message, [dynamic error, StackTrace? stackTrace]) {
    return _logger.v(message, error, stackTrace);
  }

  static d(dynamic message, [dynamic error, StackTrace? stackTrace]) {
    return _logger.d(message, error, stackTrace);
  }

  static i(dynamic message, [dynamic error, StackTrace? stackTrace]) {
    return _logger.i(message, error, stackTrace);
  }

  static w(dynamic message, [dynamic error, StackTrace? stackTrace]) {
    return _logger.w(message, error, stackTrace);
  }

  static e(dynamic message, [dynamic error, StackTrace? stackTrace]) {
    return _logger.e(message, error, stackTrace);
  }
}
