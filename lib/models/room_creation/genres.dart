import 'package:formz/formz.dart';

enum RoomCreationGenresError { empty, full }

class Genres extends FormzInput<List<String>, RoomCreationGenresError> {
  const Genres.pure() : super.pure(const ["pop", "rock", "folk"]);

  const Genres.dirty([List<String> value = const []]) : super.dirty(value);

  @override
  RoomCreationGenresError? validator(List<String> value) {
    if (value.isEmpty) {
      return RoomCreationGenresError.empty;
    } else {
      return value.length > 5 ? RoomCreationGenresError.full : null;
    }
  }
}
