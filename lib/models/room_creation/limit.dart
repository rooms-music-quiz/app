import 'package:formz/formz.dart';

enum RoomCreationLimitError { low, high }

class Limit extends FormzInput<int, RoomCreationLimitError> {
  const Limit.pure() : super.pure(10);

  const Limit.dirty([int value = 10]) : super.dirty(value);

  @override
  RoomCreationLimitError? validator(int value) {
    return value < 1
        ? RoomCreationLimitError.low
        : value > 100
            ? RoomCreationLimitError.high
            : null;
  }
}
