import 'package:formz/formz.dart';

enum RoomCreationDifficultyError { low, high }

class Difficulty extends FormzInput<int, RoomCreationDifficultyError> {
  const Difficulty.pure() : super.pure(60);

  const Difficulty.dirty([int value = 60]) : super.dirty(value);

  @override
  RoomCreationDifficultyError? validator(int value) {
    return value < 1
        ? RoomCreationDifficultyError.low
        : value > 100
            ? RoomCreationDifficultyError.high
            : null;
  }
}
