class RoomStatus {
  static const LOBBY = "LOBBY";
  static const INITIALIZATION = "INITIALIZATION";
  static const PLAYING = "PLAYING";
  static const BUFFERING = "BUFFERING";
  static const TERMINATED = "TERMINATED";
  static const ERROR = "ERROR";
}
