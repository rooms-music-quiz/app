import 'package:room_music_quiz/models/player_result.dart';

import 'artist.dart';

class RoomRound {
  final String track;
  final List<Artist> artists;
  final List<PlayerResult> results;

  RoomRound({
    required this.track,
    required this.artists,
    required this.results,
  });
}
