import 'package:equatable/equatable.dart';

import 'artist.dart';
import 'image.dart';

class Track extends Equatable {
  final String id;
  final String name;
  final String? previewUrl;
  final Set<Artist> artists;
  final Set<Image> images;

  const Track({
    required this.id,
    required this.name,
    required this.previewUrl,
    required this.artists,
    required this.images,
  });

  factory Track.fromJson(Map<String, dynamic> json) {
    return Track(
      id: json["id"]! as String,
      name: json["name"]! as String,
      previewUrl: json["previewUrl"],
      artists: (json["artists"]! as List<dynamic>)
          .map((e) => Artist.fromJson(e))
          .toSet(),
      images: (json["images"]! as List<dynamic>)
          .map((e) => Image.fromJson(e))
          .toSet(),
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [id, name, previewUrl, artists];
}
