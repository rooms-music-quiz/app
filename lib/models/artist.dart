class Artist {
  final String id;
  final String name;

  const Artist({
    required this.id,
    required this.name,
  });

  factory Artist.fromJson(Map<String, dynamic> json) {
    return Artist(
      id: json["id"]! as String,
      name: json["name"]! as String,
    );
  }
}
