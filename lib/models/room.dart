import 'package:equatable/equatable.dart';
import 'package:room_music_quiz/models/track.dart';

import 'playlist.dart';

class Room extends Equatable {
  final String id;
  final Playlist playlist;
  final Track? currentTrack;
  final String state;

  const Room({
    required this.id,
    required this.playlist,
    required this.state,
    this.currentTrack,
  });

  factory Room.fromJson(Map<String, dynamic> json) {
    final currentTrack = json["currentlyPlaying"] == null
        ? null
        : Track.fromJson(json["currentlyPlaying"]);
    return Room(
      id: json["id"]! as String,
      playlist: Playlist.fromJson(json["playlist"]),
      state: json["state"]! as String,
      currentTrack: currentTrack,
    );
  }

  @override
  List<Object?> get props => [id, playlist, currentTrack];
}
