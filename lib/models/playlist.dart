import 'package:equatable/equatable.dart';

import 'image.dart';
import 'track.dart';

class Playlist extends Equatable {
  final String id;
  final String name;
  final String description;
  final Set<Track> tracks;
  final Set<Image>? images;

  const Playlist({
    required this.id,
    required this.name,
    required this.description,
    required this.tracks,
    this.images,
  });

  factory Playlist.fromJson(Map<String, dynamic> json) {
    return Playlist(
      id: json["id"]! as String,
      name: json["name"]! as String,
      description: json["description"]! as String,
      tracks: (json["tracks"]! as List<dynamic>)
          .map((e) => Track.fromJson(e))
          .toSet(),
      images: (json["images"] as List<dynamic>?)
          ?.map((e) => Image.fromJson(e))
          .toSet(),
    );
  }

  @override
  List<Object?> get props => [id, name, description, tracks];
}
