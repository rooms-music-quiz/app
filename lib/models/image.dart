class Image {
  final String url;
  final int? width;
  final int? height;

  const Image({
    required this.url,
    this.width,
    this.height,
  });

  factory Image.fromJson(Map<String, dynamic> json) {
    return Image(
      url: json["url"]! as String,
      width: json["width"],
      height: json["height"],
    );
  }
}
