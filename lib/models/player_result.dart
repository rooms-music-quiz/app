import 'package:equatable/equatable.dart';

class PlayerResult extends Equatable {
  final String? playerId;
  final double title;
  final List<double> artists;

  static const empty = PlayerResult(playerId: null, title: -0.1, artists: []);

  const PlayerResult({
    this.playerId,
    required this.title,
    required this.artists,
  });

  factory PlayerResult.fromJson(Map<String, dynamic> json) => PlayerResult(
        title: json["track"] as double,
        artists:
            (json["artists"] as List<dynamic>).map((e) => e as double).toList(),
      );

  @override
  List<Object?> get props => [playerId, title, artists];
}
