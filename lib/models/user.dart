import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String id;
  final bool emailVerified;

  final String? email;
  final String? name;
  final String? phoneNumber;
  final String? previewUrl;

  const User({
    required this.id,
    required this.emailVerified,
    this.email,
    this.name,
    this.phoneNumber,
    this.previewUrl,
  });

  static const empty = User(
    id: "",
    emailVerified: false,
  );

  @override
  List<Object?> get props => [id];
}
