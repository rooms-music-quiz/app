import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:room_music_quiz/features/room_list/room_list_bloc.dart';
import 'package:room_music_quiz/router/router_delegate.dart';
import 'package:room_music_quiz/utils/logger.dart';

class RoomListView extends StatefulWidget {
  const RoomListView({Key? key}) : super(key: key);

  @override
  State<RoomListView> createState() => _RoomListViewState();
}

class _RoomListViewState extends State<RoomListView> {
  final _refreshIndicatorState = RefreshIndicatorState();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RoomListBloc, RoomListState>(
      builder: (context, state) {
        switch (state.status) {
          case RoomListStatus.initial:
            return const Center(child: CircularProgressIndicator());
          case RoomListStatus.failure:
            return const Center(child: Text('failed to fetch rooms'));
          case RoomListStatus.success:
            if (state.rooms.isEmpty) {
              return RefreshIndicator(
                onRefresh: () async {
                  context.read<RoomListBloc>().add(RoomListFetched());
                },
                child: const Center(
                  child: Text('No rooms'),
                ),
              );
            } else {
              return RefreshIndicator(
                onRefresh: () async {
                  context.read<RoomListBloc>().add(RoomListFetched());
                },
                child: ListView.builder(
                  physics: const AlwaysScrollableScrollPhysics(),
                  itemCount: state.rooms.length,
                  itemBuilder: (context, index) {
                    final room = state.rooms.elementAt(index);
                    return ListTile(
                      leading:
                          Image.network(room.playlist.images?.first.url ?? ""),
                      title: Text(room.playlist.name),
                      isThreeLine: true,
                      subtitle: Text(room.playlist.description),
                      onTap: () {
                        MyRouterDelegate().pushPage(name: "/room/${room.id}");
                      },
                      trailing: const Icon(Icons.navigate_next),
                    );
                  },
                ),
              );
            }
        }
      },
    );
  }
}
