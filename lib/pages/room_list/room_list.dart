import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:room_music_quiz/features/room_list/room_list_bloc.dart';
import 'package:room_music_quiz/pages/room_list/room_list_view.dart';
import 'package:room_music_quiz/repositories/room_repository.dart';

class RoomList extends StatelessWidget {
  const RoomList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => RoomListBloc(roomRepository: RoomRepository())
        ..add(RoomListFetched()),
      child: const RoomListView(),
    );
  }
}
