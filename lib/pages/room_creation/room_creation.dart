import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:room_music_quiz/features/room_creation/room_creation_bloc.dart';
import 'package:room_music_quiz/pages/room_creation/room_creation_view.dart';
import 'package:room_music_quiz/repositories/room_repository.dart';

class RoomCreation extends StatelessWidget {
  const RoomCreation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RoomCreationBloc(RoomRepository()),
      child: const RoomCreationView(),
    );
  }
}
