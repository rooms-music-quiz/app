import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:room_music_quiz/features/room_creation/room_creation_bloc.dart';
import 'package:room_music_quiz/router/router_delegate.dart';

class RoomCreationView extends StatelessWidget {
  const RoomCreationView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<RoomCreationBloc, RoomCreationState>(
      listener: (context, state) {
        if (state.status.isSubmissionFailure) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              const SnackBar(content: Text('Could not create the Room')),
            );
        } else if (state.status.isSubmissionSuccess) {
          MyRouterDelegate().replace(name: "/room/${state.roomId}");
        }
      },
      child: Align(
        alignment: const Alignment(0, -1 / 3),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildTitle(),
            _buildGenres(context),
            _buildDifficulty(context),
            _buildLimit(context),
            _buildCreateButton(context)
          ],
        ),
      ),
    );
  }

  Widget _buildTitle() => const Center(
        child: Text("Room Creation"),
      );

  Widget _buildGenres(BuildContext context) =>
      BlocBuilder<RoomCreationBloc, RoomCreationState>(
        buildWhen: (previous, current) => previous.genres != current.genres,
        builder: (context, state) => Wrap(
          spacing: 16,
          children: [
            FilterChip(
              label: const Text("Pop"),
              selected: state.genres.value.contains("pop"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("pop");
                } else {
                  genres.remove("pop");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
            FilterChip(
              label: const Text("Rock"),
              selected: state.genres.value.contains("rock"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("rock");
                } else {
                  genres.remove("rock");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
            FilterChip(
              label: const Text("Electro"),
              selected: state.genres.value.contains("electro"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("electro");
                } else {
                  genres.remove("electro");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
            FilterChip(
              label: const Text("Metal"),
              selected: state.genres.value.contains("metal"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("metal");
                } else {
                  genres.remove("metal");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
            FilterChip(
              label: const Text("Folk"),
              selected: state.genres.value.contains("folk"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("folk");
                } else {
                  genres.remove("folk");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
            FilterChip(
              label: const Text("EDM"),
              selected: state.genres.value.contains("edm"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("edm");
                } else {
                  genres.remove("edm");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
            FilterChip(
              label: const Text("Hip-Hop"),
              selected: state.genres.value.contains("hip-hop"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("hip-hop");
                } else {
                  genres.remove("hip-hop");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
            FilterChip(
              label: const Text("Jazz"),
              selected: state.genres.value.contains("jazz"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("jazz");
                } else {
                  genres.remove("jazz");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
            FilterChip(
              label: const Text("Reggae"),
              selected: state.genres.value.contains("reggae"),
              onSelected: (bool isSelected) {
                final genres = state.genres.value.toList();
                if (isSelected) {
                  genres.add("reggae");
                } else {
                  genres.remove("reggae");
                }
                context
                    .read<RoomCreationBloc>()
                    .add(RoomCreationGenresUpdated(genres));
              },
            ),
          ],
        ),
      );

  Widget _buildDifficulty(BuildContext context) =>
      BlocBuilder<RoomCreationBloc, RoomCreationState>(
        buildWhen: (previous, current) =>
            previous.difficulty != current.difficulty,
        builder: (context, state) => Slider(
          label: "Difficulty",
          value: state.difficulty.value.toDouble(),
          min: 1,
          max: 75,
          onChanged: (double value) {
            context
                .read<RoomCreationBloc>()
                .add(RoomCreationDifficultyUpdated(value.toInt()));
          },
        ),
      );

  Widget _buildLimit(BuildContext context) =>
      BlocBuilder<RoomCreationBloc, RoomCreationState>(
        buildWhen: (previous, current) => previous.limit != current.limit,
        builder: (context, state) => Slider(
          label: "Number of tracks",
          value: state.limit.value.toDouble(),
          min: 1,
          max: 100,
          onChanged: (double value) {
            context
                .read<RoomCreationBloc>()
                .add(RoomCreationLimitUpdated(value.toInt()));
          },
        ),
      );

  Widget _buildCreateButton(BuildContext context) =>
      BlocBuilder<RoomCreationBloc, RoomCreationState>(
        buildWhen: (previous, current) => previous.status != current.status,
        builder: (context, state) => state.status.isSubmissionInProgress
            ? const CircularProgressIndicator()
            : ElevatedButton(
                child: const Text('Create'),
                onPressed: state.status.isValidated || state.status.isPure
                    ? () {
                        context
                            .read<RoomCreationBloc>()
                            .add(const RoomCreationSubmitted());
                      }
                    : null,
              ),
      );
}
