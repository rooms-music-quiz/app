import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:room_music_quiz/features/room/room_bloc.dart';
import 'package:room_music_quiz/models/room_status.dart';
import 'package:room_music_quiz/pages/room/room_round_result.dart';
import 'package:room_music_quiz/pages/room/widget/guessing_textfield.dart';
import 'package:room_music_quiz/pages/room/widget/live_result.dart';

class RoomView extends StatelessWidget {
  const RoomView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RoomBloc, RoomState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        switch (state.status) {
          case RoomStatus.LOBBY:
            return Center(
              child: ElevatedButton(
                onPressed: () => context.read<RoomBloc>().add(RoomStartEvent()),
                child: const Text("Start"),
              ),
            );
          case RoomStatus.PLAYING:
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  GuessingTextField(),
                  LiveResult(),
                ],
              ),
            );
          case RoomStatus.BUFFERING:
            return RoomRoundResult(track: state.previousSongs.last);
          default:
            return const Center(
              child: Text("Le jeu est sur le point de commencer..."),
            );
        }
      },
    );
  }
}
