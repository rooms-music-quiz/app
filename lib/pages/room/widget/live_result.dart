import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:room_music_quiz/features/room/room_bloc.dart';
import 'package:room_music_quiz/models/player_result.dart';
import 'package:room_music_quiz/models/track.dart';

class LiveResult extends StatelessWidget {
  const LiveResult({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RoomBloc, RoomState>(
        buildWhen: (previous, current) =>
            previous.roundResult != current.roundResult,
        builder: (context, state) {
          final Track currentSong = state.currentlyPlaying!;
          final PlayerResult? roundResult = state.roundResult;
          final String title = roundResult != null && roundResult.title > 0.8
              ? currentSong.name
              : "????";
          final List<String> artists =
              currentSong.artists.map((e) => "????").toList();
          if (roundResult != null) {
            for (var i = 0; i < roundResult.artists.length; i++) {
              if (roundResult.artists.elementAt(i) > 0.8) {
                artists.replaceRange(
                    i, i + 1, [currentSong.artists.elementAt(i).name]);
              }
            }
          }

          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "Title : $title",
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              Wrap(
                spacing: 16,
                children: artists
                    .map(
                      (e) => RawChip(
                        label: Text(e),
                      ),
                    )
                    .toList(),
              )
            ],
          );
        });
  }
}
