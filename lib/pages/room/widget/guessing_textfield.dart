import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:room_music_quiz/features/room/room_bloc.dart';

class GuessingTextField extends StatelessWidget {
  const GuessingTextField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RoomBloc, RoomState>(
      buildWhen: (previous, current) =>
          previous.guessStatus != current.guessStatus,
      builder: (context, state) {
        final isLoading = state.guessStatus == FormzStatus.submissionInProgress;
        bool? isTitleValid, isArtistValid;
        String? errorText;
        if (state.guessStatus == FormzStatus.submissionSuccess) {
          isTitleValid = state.roundResult.title > 0.8;
          isArtistValid = state.roundResult.artists.any((it) => it > 0.8);
          if (state.roundResult.title <= 0.8 &&
              state.roundResult.title > 0.65) {
            errorText = "Pas loin du bon titre !";
          } else if (state.roundResult.artists
              .any((e) => e <= 0.8 && e > 0.65)) {
            errorText = "Pas loin de l'artiste !";
          } else if (isTitleValid == false && isArtistValid == false) {
            errorText = "C'est non";
          }
        }
        return TextFormField(
          autofocus: true,
          autocorrect: false,
          enabled: !isLoading,
          showCursor: !isLoading,
          enableSuggestions: false,
          textInputAction: TextInputAction.send,
          decoration: InputDecoration(
            errorText: errorText,
            enabled: !isLoading,
            suffixIcon: isLoading
                ? const Align(
                    heightFactor: 0.2,
                    widthFactor: 0.2,
                    child: CircularProgressIndicator(),
                  )
                : null,
            border: const OutlineInputBorder(),
          ),
          onFieldSubmitted: (guess) {
            context.read<RoomBloc>().add(RoomGuessEvent(guess));
          },
        );
      },
    );
  }
}
