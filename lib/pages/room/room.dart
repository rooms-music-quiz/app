import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:room_music_quiz/features/room/room_bloc.dart';
import 'package:room_music_quiz/pages/room/room_view.dart';
import 'package:room_music_quiz/repositories/auth_repository.dart';
import 'package:room_music_quiz/repositories/room_repository.dart';
import 'package:room_music_quiz/repositories/track_repository.dart';

class Room extends StatelessWidget {
  const Room({Key? key, required this.id}) : super(key: key);

  final String id;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          RoomBloc(id, RoomRepository(), AuthRepository(), TrackRepository()),
      child: const RoomView(),
    );
  }
}
