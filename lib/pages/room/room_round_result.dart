import 'package:flutter/material.dart';
import 'package:room_music_quiz/models/track.dart';

class RoomRoundResult extends StatelessWidget {
  const RoomRoundResult({Key? key, required track})
      : _track = track,
        super(key: key);

  final Track _track;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.network(_track.images.first.url),
          Text(
            _track.name,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          Text(
            _track.artists.map((e) => e.name).join(", "),
            style: const TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}
