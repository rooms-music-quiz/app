import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:room_music_quiz/repositories/auth_repository.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authRepository = RepositoryProvider.of<AuthRepository>(context);

    return Center(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ElevatedButton(
            onPressed: () =>
                authRepository.login(AuthenticationMethods.anonymous),
            child: const Text("Anonymous"),
          ),
          ElevatedButton(
            onPressed: () => authRepository.login(AuthenticationMethods.google),
            child: const Text("Google"),
          ),
          ElevatedButton(
            onPressed: () =>
                authRepository.login(AuthenticationMethods.facebook),
            child: const Text("Facebook"),
          ),
        ],
      ),
    );
  }
}
