import 'package:flutter/material.dart';
import 'package:room_music_quiz/models/playlist.dart';
import 'package:room_music_quiz/repositories/auth_repository.dart';
import 'package:room_music_quiz/repositories/playlist_repository.dart';

class FeaturedPlaylist extends StatelessWidget {
  const FeaturedPlaylist({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Set<Playlist>>(
      future: PlaylistRepository(locale: Localizations.localeOf(context))
          .getFeaturedPlaylists(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Center(
            child: Text('An error has occurred!'),
          );
        } else if (snapshot.hasData) {
          return PlaylistList(playlists: snapshot.data!);
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class PlaylistList extends StatelessWidget {
  const PlaylistList({Key? key, required this.playlists}) : super(key: key);

  final Set<Playlist> playlists;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
      ),
      itemCount: playlists.length + 1,
      itemBuilder: (context, index) {
        if (index == playlists.length) {
          return ElevatedButton(
            onPressed: () => AuthRepository().logout(),
            child: const Text("Logout"),
          );
        } else {
          return Ink.image(
            image: NetworkImage(playlists.elementAt(index).images?.first.url ?? ""),
          );
        }
      },
    );
  }
}
