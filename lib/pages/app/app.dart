import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:room_music_quiz/features/authentication/authentication_bloc.dart';
import 'package:room_music_quiz/pages/app/app_view.dart';
import 'package:room_music_quiz/repositories/auth_repository.dart';
import 'package:room_music_quiz/repositories/user_repository.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: AuthRepository(),
      child: BlocProvider(
        create: (_) => AuthenticationBloc(
          authenticationRepository: AuthRepository(),
          userRepository: UserRepository(),
        ),
        child: const AppView(),
      ),
    );
  }
}
