import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:room_music_quiz/features/authentication/authentication_bloc.dart';
import 'package:room_music_quiz/repositories/auth_repository.dart';
import 'package:room_music_quiz/router/router_delegate.dart';

class AppView extends StatefulWidget {
  const AppView({Key? key}) : super(key: key);

  @override
  State<AppView> createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final routerDelegate = MyRouterDelegate()..pushPage(name: "/");

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rooms Music Quiz',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', 'UK'),
        Locale('en', 'US'),
        Locale('fr', 'FR'),
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: BlocListener<AuthenticationBloc, AuthenticationState>(
          listener: (BuildContext context, AuthenticationState state) {
            switch (state.status) {
              case AuthenticationStatus.unknown:
                routerDelegate.pushPage(name: "/");
                break;
              case AuthenticationStatus.authenticated:
                routerDelegate.replaceAll(name: "/rooms");
                break;
              case AuthenticationStatus.unauthenticated:
                routerDelegate.replaceAll(name: "/login");
                break;
            }
          },
          child: Router(
            routerDelegate: routerDelegate,
            backButtonDispatcher: RootBackButtonDispatcher(),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => routerDelegate.pushPage(name: "rooms/creations"),
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
